// Sample Functions to Test
const getCircleArea = (r) =>  +(Math.PI * (r ** 2)).toFixed(2);

const checkIfPassed = (score, total) => (score / total) * 100 >= 75;

const getAverage = (num1, num2, num3, num4) => (num1 + num2 + num3 + num4) / 4;

const getSum = (num1, num2) => num1 + num2;

const getDifference = (num1, num2) => num1 - num2;

const factorial = (n) => {

    if ( typeof n !== "number" || n < 0 ) return undefined;
    if ( n === 0 ) return 1;
    if ( n === 1 ) return 1;

    return n * factorial(n - 1);

}

// Acitivty S2
const div_check = (n) => {

    if (n < 5 || n < 7) return false;
    if (typeof n !== "number") return undefined;

    return n % 5 === 0 || n % 7 === 0

};





module.exports = {
    getCircleArea: getCircleArea,
    checkIfPassed: checkIfPassed,
    getAverage: getAverage,
    getSum: getSum,
    getDifference: getDifference,
    factorial: factorial,
    div_check: div_check
}