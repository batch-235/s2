Mocha is a testing framework that provides functions that are
 executed according to a specific order , and that logs their
           results to the terminal window .

When you read tests written in Mocha , you'll see regular use of
           the keywords describe and it .
These provide structure to the tests by batching them into test suites and test cases .


A test suite is a collection of tests all relating to a single
          functionality or behavior .

A test case or a unit test is a single description about the
desired behavior of the code that either passes or fails .

Chai is an assertion library that is often used alongside Mocha .
It provides functions and methods that help you compare the
       output of a certain test with its expected value .

Chai provides clean syntax that almost reads like English .

Example of a Chai assertion :
expect ( exampleArray ) .to.have.lengthOf( 3 ) ;
This code will check whether that the variable exampleArray has a length of three or not .
                        
Assert Vs expect
The assert style is exposed through assert interface . This
       provides the classic assert - dot notation
Assert style allows you to include an optional message as the
last parameter in the assert statement . These will be included
   in the error messages should your assertion not pass .

Expect style allows you to chain together natural language
                   assertions .
Expect also allows you to include arbitrary messages to
  prepend to any failed assertions that might occur .

Expect Language Chains
