const { assert, expect } = require("chai");
const { describe } = require("mocha");
const { getCircleArea, checkIfPassed, getAverage, getSum, getDifference, factorial, div_check } = require("../util");

describe("Test getCircleArea", () => {

    it("[1] Test area of circle radius 15 is 706.86", () => {
        let area = getCircleArea(15);
        assert.equal(area, 706.86);
    })

    it("[2] Test area of circle with radius of 9 is 254.47", () => {
        let area = getCircleArea(9);
        expect(area).to.equal(254.47);
    })

})

describe("Test checkIfPassed", () => {

    it("[1] Test 25 out of 30 is passed", () => {
        let isPassed = checkIfPassed(25,30);
        assert.equal(isPassed, true);
    })

    it("[2] Test 30 out of 50 is not passed", () => {
        let isPassed = checkIfPassed(30,50);
        assert.equal(isPassed, false);
    })

})

describe("Test getAverage", () => {

    it("[1] Test 80, 82, 84 and 86 to get avarage of 83", () => {
        let average = getAverage(80, 82, 84, 86);
        assert.equal(average, 83);
    })

    it("[2] Test 70, 80, 82 and 84 to get avarage of 79", () => {
        let average = getAverage(70, 80, 82, 84);
        assert.equal(average, 79);
    })

})

describe("Test getSum", () => {

    it("[1] Test sum of 15 and 30 is 45", () => {
        assert.equal(getSum(15,30), 45);
    })

    it("[2] Test sum of 25 and 50 is 75", () => {
        assert.equal(getSum(25,50), 75);
    })

})

describe("Test getDifference", () => {

    it("[1] Difference of 70 and 40 is 30", () => {
        assert.equal(getDifference(70,40), 30)
    })

    it("[2] Difference of 125 and 50 is 75", () => {
        assert.equal(getDifference(125,50), 75)
    })

})

describe("Test factorial", () => {

    it("[1] Test that 5! is 120", () => {
        expect(factorial(5)).to.equal(120);
    })

    it("[2] Test that 1! is 1", () => {
        expect(factorial(1)).to.equal(1);
    })

    it("[3] Test that 0! is 1", () => {
        assert.equal(factorial(0), 1);
    })

    it("[4] Test negative factorial is undefined", () => {
        expect(factorial(-1)).to.equal(undefined);
    })

    it("[5] Test that non-numeric value returns an error", () => {
        expect(factorial("1")).to.equal(undefined);
    })

})

// Activity S2
describe('Test div_check', () => {

    it("[1] 120 should be divisible by 5", () => {
        assert.equal(div_check(120), true);
    })

    it("[2] 14 should be divisible by 7", () => {
        assert.equal(div_check(14), true);
    })

    it("[3] 105 should be divisible by 5 OR 7", () => {
        assert.equal(div_check(105), true);
    })

    it("[4] 22 should be divisible by 5 OR 7", () => {
        assert.notEqual(div_check(22), true);
    })

    it("[additional test] Argument must be a valid number data type", () => {
        expect(div_check("120")).to.equal(undefined);
    })

})